drop table if exists customer;

drop sequence if exists customer_seq;

CREATE SEQUENCE customer_seq increment by 50;

create table if not exists customer
(
    id  bigint default NEXTVAL('customer_seq'),
    cpf varchar(14) not null
);

create
    unique index customer_cpf_uindex
    on customer (cpf);

-- insert into customer(cpf)
-- values ('10165182008');