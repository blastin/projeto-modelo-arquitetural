package br.project.modelproject.application.customer;

@FunctionalInterface
public interface UseCaseSaveCustomer {

    void save(final SaveCustomerRequest saveCustomerRequest);

}
