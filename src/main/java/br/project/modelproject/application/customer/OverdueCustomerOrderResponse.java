package br.project.modelproject.application.customer;

import java.math.BigDecimal;

public interface OverdueCustomerOrderResponse {

    String getDescription();

    BigDecimal getValue();

}
