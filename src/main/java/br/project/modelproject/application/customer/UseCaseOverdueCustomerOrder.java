package br.project.modelproject.application.customer;

import java.util.Collection;

@FunctionalInterface
public interface UseCaseOverdueCustomerOrder {

    Collection<OverdueCustomerOrderResponse> withValueAboveTheLimit(final String cpf);

}
