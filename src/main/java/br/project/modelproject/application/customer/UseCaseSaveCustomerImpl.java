package br.project.modelproject.application.customer;

import br.project.modelproject.domain.customer.CustomersCommand;
import br.project.modelproject.domain.customer.CustomersQuery;

public final class UseCaseSaveCustomerImpl implements UseCaseSaveCustomer {

    public UseCaseSaveCustomerImpl
            (
                    final CustomersQuery customersQuery,
                    final CustomersCommand customersCommand
            ) {
        this.customersQuery = customersQuery;
        this.customersCommand = customersCommand;
    }

    private final CustomersQuery customersQuery;

    private final CustomersCommand customersCommand;

    @Override
    public void save(final SaveCustomerRequest saveCustomerRequest) {
        saveCustomerRequest
                .toCustomer()
                .save(customersCommand, customersQuery);
    }

}
