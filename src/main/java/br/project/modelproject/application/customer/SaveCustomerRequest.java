package br.project.modelproject.application.customer;

import br.project.modelproject.domain.customer.Customer;

public interface SaveCustomerRequest {

    String getCpf();

    default Customer toCustomer() {
        return new Customer(getCpf());
    }

}
