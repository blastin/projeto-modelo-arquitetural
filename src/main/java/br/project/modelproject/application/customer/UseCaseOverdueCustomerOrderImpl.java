package br.project.modelproject.application.customer;

import br.project.modelproject.domain.customer.Customer;
import br.project.modelproject.domain.customer.CustomersQuery;
import br.project.modelproject.domain.order.Orders;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.stream.Collectors;

public final class UseCaseOverdueCustomerOrderImpl implements UseCaseOverdueCustomerOrder {

    public UseCaseOverdueCustomerOrderImpl(final CustomersQuery customersQuery, final Orders orders) {
        this.customersQuery = customersQuery;
        this.orders = orders;
    }

    private final Orders orders;

    private final CustomersQuery customersQuery;

    private static final BigDecimal VALOR_200 = BigDecimal.valueOf(200);

    @Override
    public Collection<OverdueCustomerOrderResponse> withValueAboveTheLimit(final String cpf) {
        return new Customer(cpf)
                .overdueCustomerOrders(customersQuery, orders)
                .stream()
                .filter(order -> order.isValueGreaterThan(VALOR_200))
                .map
                        (
                                order -> new OverdueCustomerOrderResponse() {
                                    @Override
                                    public String getDescription() {
                                        return order.getDescription();
                                    }

                                    @Override
                                    public BigDecimal getValue() {
                                        return order.getValue();
                                    }
                                }
                        )
                .collect(Collectors.toList());
    }

}
