package br.project.modelproject.domain.customer;

public abstract class CustomerException extends RuntimeException {

    public abstract int toInt();

}
