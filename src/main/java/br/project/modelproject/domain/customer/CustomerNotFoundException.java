package br.project.modelproject.domain.customer;

public final class CustomerNotFoundException extends CustomerException {

    /**
     * Utilize padrão format e argumentos
     *
     * @param cpf cpf como argumento
     */
    CustomerNotFoundException(final String cpf) {
        this.cpf = cpf;
    }

    private final String cpf;

    @Override
    public int toInt() {
        return 4384;
    }

    @Override
    public String getMessage() {
        return String.format("cliente (%s) não foi encontrado", cpf);
    }

}
