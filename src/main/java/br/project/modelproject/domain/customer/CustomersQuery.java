package br.project.modelproject.domain.customer;

/**
 * Customers apenas possui um comportamento de contrato
 */
@FunctionalInterface
public interface CustomersQuery {

    boolean existsByCpf(final String cpf);

}
