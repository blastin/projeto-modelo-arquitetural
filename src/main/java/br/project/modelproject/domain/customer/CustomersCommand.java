package br.project.modelproject.domain.customer;

@FunctionalInterface
public interface CustomersCommand {

    void save(final Customer customer);

}
