package br.project.modelproject.domain.customer;

import br.project.modelproject.domain.order.Order;
import br.project.modelproject.domain.order.Orders;

import java.util.Collection;
import java.util.stream.Collectors;

public final class Customer {

    public static Customer mock() {
        return new Customer("219.786.850-09");
    }

    private static final NumeroContribuinte.PredicadoCPF PREDICADO =
            new NumeroContribuinte.PredicadoCPF();

    public Customer(final String cpf) {
        if (!PREDICADO.test(cpf))
            throw new CustomerInvalidException(cpf);
        this.cpf = cpf;
    }

    private final String cpf;

    public Collection<Order> overdueCustomerOrders
            (
                    final CustomersQuery customersQuery,
                    final Orders orders
            ) {

        if (customersQuery.existsByCpf(getCpf())) {
            return orders
                    .all(getCpf())
                    .stream()
                    .filter(Order::overdue)
                    .collect(Collectors.toList());
        }

        throw new CustomerNotFoundException(cpf);

    }

    public void save(final CustomersCommand customersCommand, final CustomersQuery customersQuery) {

        if (customersQuery.existsByCpf(getCpf())) {
            throw new CustomerFoundException(cpf);
        }

        customersCommand.save(this);

    }

    public String getCpf() {
        return cpf;
    }

}
