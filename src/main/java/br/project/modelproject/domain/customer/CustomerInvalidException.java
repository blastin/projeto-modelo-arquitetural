package br.project.modelproject.domain.customer;

public final class CustomerInvalidException extends CustomerException {

    /**
     * Utilize padrão format e argumentos
     *
     * @param cpf cpf como argumento
     */
    CustomerInvalidException(final String cpf) {
        this.cpf = cpf;
    }

    private final String cpf;

    @Override
    public int toInt() {
        return 2932;
    }

    @Override
    public String getMessage() {
        return String.format("CPF (%s) incorreto", cpf);
    }

}
