package br.project.modelproject.domain.customer;

import java.util.function.Predicate;
import java.util.regex.Pattern;

abstract class NumeroContribuinte implements Predicate<String> {

    private NumeroContribuinte
            (
            ) {
        this.tamanhoCadeia = 11;
        this.basePrimeiroDigito = 11;
        this.baseSegundoDigito = 12;
    }

    private final int tamanhoCadeia;
    private final int basePrimeiroDigito;
    private final int baseSegundoDigito;

    @Override
    public boolean test(final String cadeiaNumeroContribuinte) {

        if (cadeiaNumeroContribuinte == null) return false;

        if (!matchExpressaoRegular(cadeiaNumeroContribuinte)) return false;

        final Integer[] numeroContribuinte = CalculoModulo11.transformarCadeiaDeCaracteresEmUmArray(cadeiaNumeroContribuinte);

        if (digitosIguais(numeroContribuinte)) return false;

        return digitosVerificadoresValidos(numeroContribuinte);

    }

    private boolean digitosVerificadoresValidos(final Integer[] numeroContribuinte) {

        final int primeiroDigito = CalculoModulo11.calcularDigitoVerificador(numeroContribuinte, basePrimeiroDigito, tamanhoCadeia - 2);

        if (primeiroDigitoVerificadorIncorreto(numeroContribuinte, primeiroDigito)) return false;

        final int segundoDigito = CalculoModulo11.calcularDigitoVerificador(numeroContribuinte, baseSegundoDigito, tamanhoCadeia - 1);

        return segundoDigitoVerificadorCorreto(numeroContribuinte, segundoDigito);

    }

    private boolean digitosIguais(Integer[] digitos) {

        var i = 1;

        while (i < tamanhoCadeia) {

            if (!digitos[0].equals(digitos[i++])) return false;

        }

        return true;

    }

    private boolean primeiroDigitoVerificadorIncorreto(final Integer[] numeroContribuinte, final int digitoVerificador) {

        final int indice = indiceDoPrimeiroDigitoVerificador();

        return CalculoModulo11.digitoVerificadorIncorreto(indice, numeroContribuinte, digitoVerificador);

    }

    private boolean segundoDigitoVerificadorCorreto(final Integer[] numeroContribuinte, final int digitoVerificador) {

        final int indice = indiceDoSegundoDigitoVerificador();

        return !CalculoModulo11.digitoVerificadorIncorreto(indice, numeroContribuinte, digitoVerificador);

    }

    /**
     * @param numeroContribuinte referente a cadeia de caracteres do numero do contribuinte
     * @return caso ocorra um match com expressao regular, retornara true
     */
    protected abstract boolean matchExpressaoRegular(final String numeroContribuinte);

    /**
     * Hook Method
     *
     * @return indice do primeiro digito é necessário para pesquisar no array o valor referente ao digito
     * verificador
     */
    protected abstract int indiceDoPrimeiroDigitoVerificador();

    /**
     * Hook Method
     *
     * @return indice do segundo digito é necessário para pesquisar no array o valor referente ao digito
     * verificador
     */
    protected abstract int indiceDoSegundoDigitoVerificador();

    static class PredicadoCPF extends NumeroContribuinte {

        private static final Pattern EXPRESSAO_REGULAR_CPF =
                Pattern.compile("^\\d{3}\\.?\\d{3}\\.?\\d{3}-?\\d{2}$", Pattern.CASE_INSENSITIVE);

        PredicadoCPF() {
            super();
        }

        @Override
        protected boolean matchExpressaoRegular(final String numeroContribuinte) {
            return EXPRESSAO_REGULAR_CPF.matcher(numeroContribuinte).matches();
        }

        @Override
        protected int indiceDoPrimeiroDigitoVerificador() {
            return 10;
        }

        @Override
        protected int indiceDoSegundoDigitoVerificador() {
            return 11;
        }

    }

}

