package br.project.modelproject.domain.customer;

public final class CustomerFoundException extends CustomerException {

    public CustomerFoundException(final String cpf) {
        this.cpf = cpf;
    }

    private final String cpf;

    @Override
    public int toInt() {
        return 2912;
    }

    @Override
    public String getMessage() {
        return String.format("Cliente com cpf (%s) foi encontrado", cpf);
    }

}
