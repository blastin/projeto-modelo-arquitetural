package br.project.modelproject.domain.order;

import java.util.Collection;

public interface Orders {

    Collection<Order> all(final String cpf);

}
