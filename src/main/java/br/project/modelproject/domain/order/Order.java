package br.project.modelproject.domain.order;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

public class Order {

    public static OrderBuilder builder() {
        return new OrderBuilder();
    }

    public static Order mock() {
        return Order
                .builder()
                .toDatePayment(LocalDate.now().minusMonths(4))
                .withDescription("Object Thinking")
                .withValue(BigDecimal.valueOf(306.22))
                .build();
    }

    public static final class OrderBuilder {

        private OrderBuilder() {
        }

        private OrderBuilder(final Order order) {
            value = order.getValue();
            description = order.getDescription();
            datePayment = order.getDatePayment();
        }

        private BigDecimal value;

        public OrderBuilder withValue(final BigDecimal value) {
            this.value = value;
            return this;
        }

        private String description;

        public OrderBuilder withDescription(final String description) {
            this.description = description;
            return this;
        }

        private LocalDate datePayment;

        public OrderBuilder toDatePayment(final LocalDate datePayment) {
            this.datePayment = datePayment;
            return this;
        }

        public Order build() {
            return new Order(this);
        }

    }

    private Order(final OrderBuilder orderBuilder) {
        this.orderBuilder = orderBuilder;
    }

    private final OrderBuilder orderBuilder;

    public OrderBuilder toBuilder() {
        return new OrderBuilder(this);
    }

    public BigDecimal getValue() {
        return orderBuilder.value;
    }

    public String getDescription() {
        return orderBuilder.description;
    }

    public LocalDate getDatePayment() {
        return orderBuilder.datePayment;
    }

    /**
     * Não pode ser superior a 1 mês
     *
     * @return Caso seja superior a 1 mês, será considerado uma ordem em atraso
     */
    public boolean overdue() {
        return Math.abs
                (
                        Period
                                .between(orderBuilder.datePayment, LocalDate.now())
                                .getMonths()
                ) > 0;
    }

    public boolean isValueGreaterThan(final BigDecimal value) {
        return this.getValue().compareTo(value) > 0;
    }

}
