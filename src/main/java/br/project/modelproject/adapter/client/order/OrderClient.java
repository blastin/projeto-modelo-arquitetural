package br.project.modelproject.adapter.client.order;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;

@FeignClient(name = "order-client", url = "${order.url}")
interface OrderClient {

    @GetMapping(value = "orders/{cpf}", consumes = MediaType.APPLICATION_JSON_VALUE)
    Collection<OrderVeil> byCpf(@PathVariable final String cpf);

}
