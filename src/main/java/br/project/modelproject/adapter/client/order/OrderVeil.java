package br.project.modelproject.adapter.client.order;

import br.project.modelproject.domain.order.Order;

import java.math.BigDecimal;
import java.time.LocalDate;

@SuppressWarnings("unused")
class OrderVeil {

    private OrderVeil() {
        builder = Order.builder();
    }

    private final Order.OrderBuilder builder;

    private void setValue(final BigDecimal value) {
        builder.withValue(value);
    }

    private void setDescription(final String description) {
        builder.withDescription(description);
    }

    private void setDatePayment(final String datePayment) {
        builder.toDatePayment(LocalDate.parse(datePayment));
    }

    Order toOrder() {
        return builder.build();
    }

}
