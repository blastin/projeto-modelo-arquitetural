package br.project.modelproject.adapter.client.order;

import br.project.modelproject.domain.order.Orders;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients
class OrderConfig {

    @Bean
    Orders orders(final OrderClient orderClient) {
        return cpf ->
                orderClient
                        .byCpf(cpf)
                        .stream()
                        .map(OrderVeil::toOrder)
                        .toList();
    }

}
