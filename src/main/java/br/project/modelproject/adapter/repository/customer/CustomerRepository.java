package br.project.modelproject.adapter.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;

interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

    boolean existsByCpf(final String cpf);

}
