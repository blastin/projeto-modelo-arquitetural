package br.project.modelproject.adapter.repository.customer;

import br.project.modelproject.domain.customer.CustomersCommand;
import br.project.modelproject.domain.customer.CustomersQuery;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class CustomerRepositoryConfig {

    @Bean
    CustomersQuery customers(final CustomerRepository repository) {
        return repository::existsByCpf;
    }

    @Bean
    CustomersCommand customerCommand(final CustomerRepository repository) {
        return customer -> repository.save(new CustomerEntity(customer));
    }

}
