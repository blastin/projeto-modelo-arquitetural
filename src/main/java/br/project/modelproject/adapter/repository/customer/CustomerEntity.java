package br.project.modelproject.adapter.repository.customer;

import br.project.modelproject.domain.customer.Customer;

import javax.persistence.*;

@Entity
@Table(name = "customer", indexes = {
        @Index(name = "customer_cpf_uindex", columnList = "cpf", unique = true)
})
@SuppressWarnings("unused")
class CustomerEntity {

    protected CustomerEntity(){}

    CustomerEntity(final Customer customer) {
        cpf = customer.getCpf();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_sequence")
    @SequenceGenerator(name = "customer_sequence", sequenceName = "customer_seq")
    private long id;

    private String cpf;

}
