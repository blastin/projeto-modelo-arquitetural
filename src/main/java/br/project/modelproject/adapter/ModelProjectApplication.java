package br.project.modelproject.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModelProjectApplication {

    public static void main(final String[] args) {
        SpringApplication.run(ModelProjectApplication.class, args);
    }

}
