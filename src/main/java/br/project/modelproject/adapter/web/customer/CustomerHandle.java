package br.project.modelproject.adapter.web.customer;

import br.project.modelproject.domain.customer.CustomerException;
import br.project.modelproject.domain.customer.CustomerInvalidException;
import br.project.modelproject.domain.customer.CustomerNotFoundException;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
class CustomerHandle {

    @SuppressWarnings("unused")
    private static final class MessageError {

        private MessageError(final CustomerException ex) {
            this(ex.toInt(), ex.getMessage());
        }

        private MessageError(final int codigo, final String message) {
            this.codigo = codigo;
            this.message = message;
        }

        private final int codigo;

        private final String message;

        public int getCodigo() {
            return codigo;
        }

        public String getMensagem() {
            return message;
        }

    }

    @ExceptionHandler(CustomerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    MessageError customerNotFound(final CustomerNotFoundException ex) {
        return new MessageError(ex);
    }

    @ExceptionHandler(CustomerInvalidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    MessageError customerInvalid(final CustomerInvalidException ex) {
        return new MessageError(ex);
    }

    @ExceptionHandler({FeignException.NotFound.class, FeignException.ServiceUnavailable.class})
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    MessageError ordersNotFound() {
        return new MessageError(1124, "Não foi possível completar a requisição");
    }

}
