package br.project.modelproject.adapter.web.customer;

import br.project.modelproject.application.customer.UseCaseOverdueCustomerOrder;
import br.project.modelproject.application.customer.UseCaseOverdueCustomerOrderImpl;
import br.project.modelproject.domain.customer.CustomersQuery;
import br.project.modelproject.domain.order.Orders;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class CustomerConfig {

    @Bean
    UseCaseOverdueCustomerOrder useCaseOverdueCustomerOrder(final CustomersQuery customersQuery, final Orders orders) {
        return new UseCaseOverdueCustomerOrderImpl(customersQuery, orders);
    }

}
