package br.project.modelproject.adapter.web.customer;

import br.project.modelproject.application.customer.OverdueCustomerOrderResponse;
import br.project.modelproject.application.customer.UseCaseOverdueCustomerOrder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("customers")
class CustomerController {

    CustomerController(final UseCaseOverdueCustomerOrder useCaseOverdueCustomerOrder) {
        this.useCaseOverdueCustomerOrder = useCaseOverdueCustomerOrder;
    }

    private final UseCaseOverdueCustomerOrder useCaseOverdueCustomerOrder;

    @GetMapping("{cpf}/orders")
    public Collection<OverdueCustomerOrderResponse> overdueCustomerOrders(@PathVariable final String cpf) {
        return useCaseOverdueCustomerOrder.withValueAboveTheLimit(cpf);
    }

}
