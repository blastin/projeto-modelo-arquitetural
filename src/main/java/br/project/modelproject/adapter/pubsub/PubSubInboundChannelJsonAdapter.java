package br.project.modelproject.adapter.pubsub;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import com.google.cloud.spring.pubsub.integration.AckMode;
import com.google.cloud.spring.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import com.google.cloud.spring.pubsub.support.converter.JacksonPubSubMessageConverter;
import org.springframework.messaging.MessageChannel;

public final class PubSubInboundChannelJsonAdapter<T> extends PubSubInboundChannelAdapter {

    public static <T> PubSubInboundChannelAdapterBuilder<T> builder(final Class<T> clasz) {
        return new PubSubInboundChannelAdapterBuilder<>(clasz);
    }

    public static final class PubSubInboundChannelAdapterBuilder<T> {

        private PubSubInboundChannelAdapterBuilder(final Class<T> clasz) {
            this.clasz = clasz;
        }

        private final Class<T> clasz;

        private MessageChannel channel;

        public PubSubInboundChannelAdapterBuilder<T> withChannel(final MessageChannel channel) {
            this.channel = channel;
            return this;
        }

        private String subscription;

        public PubSubInboundChannelAdapterBuilder<T> toSubscription(final String subscription) {
            this.subscription = subscription;
            return this;
        }

        private PubSubTemplate template;

        public PubSubInboundChannelAdapterBuilder<T> byTemplate(final PubSubTemplate template) {
            this.template = template;
            return this;
        }

        public PubSubInboundChannelAdapter build(final ObjectMapper mapper) {
            return new PubSubInboundChannelJsonAdapter<>(this, mapper);
        }

    }

    private PubSubInboundChannelJsonAdapter(final PubSubInboundChannelAdapterBuilder<T> builder, final ObjectMapper mapper) {
        super(builder.template, builder.subscription);
        builder.template.setMessageConverter(new JacksonPubSubMessageConverter(mapper));
        setOutputChannel(builder.channel);
        setAckMode(AckMode.MANUAL);
        setPayloadType(builder.clasz);
    }

}
