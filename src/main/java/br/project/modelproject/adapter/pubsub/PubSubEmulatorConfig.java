package br.project.modelproject.adapter.pubsub;

import com.google.api.gax.core.CredentialsProvider;
import com.google.api.gax.core.NoCredentialsProvider;
import com.google.api.gax.grpc.GrpcTransportChannel;
import com.google.api.gax.rpc.FixedTransportChannelProvider;
import com.google.api.gax.rpc.TransportChannelProvider;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.cloud.pubsub.v1.TopicAdminClient;
import com.google.cloud.pubsub.v1.TopicAdminSettings;
import com.google.pubsub.v1.ProjectTopicName;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import java.io.IOException;

@Configuration
@Profile("integration")
class PubSubEmulatorConfig {

    @Bean
    @Primary
    CredentialsProvider credentialsProvider() {
        return NoCredentialsProvider.create();
    }

    @Bean
    ManagedChannel managedChannel(@Value("${spring.cloud.gcp.pubsub.emulator-host}") final String emulatorHost) {
        return ManagedChannelBuilder
                .forTarget(emulatorHost)
                .usePlaintext()
                .build();
    }

    @Bean(name = "integration")
    TransportChannelProvider transportChannelProvider(final ManagedChannel channel) {
        return FixedTransportChannelProvider
                .create(GrpcTransportChannel.create(channel));
    }

    @Bean
    TopicAdminClient topicAdminClient
            (
                    @Qualifier("integration") final TransportChannelProvider channelProvider,
                    final CredentialsProvider credentialsProvider
            ) throws IOException {
        return TopicAdminClient.create(
                TopicAdminSettings.newBuilder()
                        .setTransportChannelProvider(channelProvider)
                        .setCredentialsProvider(credentialsProvider)
                        .build());
    }

    @Bean
    ProjectTopicName projectTopicName
            (
                    @Value("${spring.cloud.gcp.pubsub.project-id}") final String projectId,
                    @Value("${customers.topic}") final String topicName
            ) {
        return ProjectTopicName
                .of(projectId, topicName);
    }

    @Bean
    Publisher createPublisher
            (
                    @Qualifier("integration") final TransportChannelProvider channelProvider,
                    final CredentialsProvider credentialsProvider,
                    final ProjectTopicName projectTopicName
            ) throws IOException {
        return Publisher
                .newBuilder(projectTopicName)
                .setChannelProvider(channelProvider)
                .setCredentialsProvider(credentialsProvider)
                .build();
    }

}
