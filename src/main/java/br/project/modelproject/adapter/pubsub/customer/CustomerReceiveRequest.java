package br.project.modelproject.adapter.pubsub.customer;

import br.project.modelproject.application.customer.SaveCustomerRequest;

final class CustomerReceiveRequest implements SaveCustomerRequest {

    private String cpf;

    @SuppressWarnings("unused")
    private void setCpf(final String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String getCpf() {
        return cpf;
    }

}
