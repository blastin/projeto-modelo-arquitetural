package br.project.modelproject.adapter.pubsub.customer;

import br.project.modelproject.application.customer.UseCaseSaveCustomer;
import com.google.cloud.spring.pubsub.support.BasicAcknowledgeablePubsubMessage;
import com.google.cloud.spring.pubsub.support.GcpPubSubHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

@Component
@Profile("integration")
class CustomerReceive {

    CustomerReceive(final UseCaseSaveCustomer useCaseSaveCustomer) {
        this.useCaseSaveCustomer = useCaseSaveCustomer;
    }

    private final UseCaseSaveCustomer useCaseSaveCustomer;

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerReceive.class);

    @ServiceActivator(inputChannel = "inputMessageChannel")
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    void messageReceiver
            (
                    final CustomerReceiveRequest customerReceiveRequest,
                    @Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) final BasicAcknowledgeablePubsubMessage message
            ) {

        LOGGER.info("Recebendo Payload {}", customerReceiveRequest);

        useCaseSaveCustomer.save(customerReceiveRequest);

        message.ack();

    }

}
