package br.project.modelproject.adapter.pubsub.customer;

import br.project.modelproject.adapter.pubsub.PubSubInboundChannelJsonAdapter;
import br.project.modelproject.application.customer.UseCaseSaveCustomer;
import br.project.modelproject.application.customer.UseCaseSaveCustomerImpl;
import br.project.modelproject.domain.customer.CustomersCommand;
import br.project.modelproject.domain.customer.CustomersQuery;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import com.google.cloud.spring.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.messaging.MessageChannel;

@Configuration
class CustomerReceiveConfig {

    private final Logger logger = LoggerFactory.getLogger(CustomerReceiveConfig.class);

    @Bean
    UseCaseSaveCustomer customer(final CustomersCommand customersCommand, final CustomersQuery customersQuery) {
        logger.info("UseCaseSaveCustomer Instance UseCaseSaveCustomerImpl {}", UseCaseSaveCustomerImpl.class);
        return new UseCaseSaveCustomerImpl(customersQuery, customersCommand);
    }

    @Bean
    @Profile("integration")
    MessageChannel inputMessageChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    @Profile("integration")
    PubSubInboundChannelAdapter inboundChannelAdapter
            (
                    @Qualifier("inputMessageChannel") final MessageChannel messageChannel,
                    final PubSubTemplate pubSubTemplate,
                    @Value("${customers.topic}") final String topic,
                    final ObjectMapper objectMapper
            ) {
        return
                PubSubInboundChannelJsonAdapter
                        .builder(CustomerReceiveRequest.class)
                        .toSubscription(topic)
                        .withChannel(messageChannel)
                        .byTemplate(pubSubTemplate)
                        .build(objectMapper);
    }

}
