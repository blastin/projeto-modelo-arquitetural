package br.project.modelproject.domain.order;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.time.LocalDate;

class OrderTest {

    @DisplayName("Quando não há atraso, deve retornar falso")
    @ParameterizedTest
    @ValueSource(longs = {1, 10, 20, 29})
    void quandoNaoHaAtrasoDeveRetornarFalso(final long dias) {
        Assertions.assertFalse
                (
                        Order
                                .mock()
                                .toBuilder()
                                .toDatePayment(LocalDate.now().minusDays(dias))
                                .build()
                                .overdue()
                );
    }

    @DisplayName("Quando há atraso, deve retornar verdadeiro")
    @ParameterizedTest
    @ValueSource(longs = {32, 40, 100})
    void quandoHaAtrasoDeveRetornarVerdadeiro(final long dias) {
        Assertions.assertTrue
                (
                        Order
                                .mock()
                                .toBuilder()
                                .toDatePayment(LocalDate.now().minusDays(dias))
                                .build()
                                .overdue()
                );
    }

    @Test
    @DisplayName("Quando valor de ordem é menor, devemos retornar falso")
    void quandoValorDeOrdemEhMenorDevemosRetornarFalso() {
        Assertions.assertFalse
                (
                        Order
                                .mock()
                                .isValueGreaterThan(BigDecimal.valueOf(400))
                );
    }

    @Test
    @DisplayName("Quando valor de ordem é maior, devemos retornar true")
    void quandoValorDeOrdemEhMaiorDevemosRetornarVerdadeiro() {
        Assertions.assertTrue
                (
                        Order
                                .mock()
                                .isValueGreaterThan(BigDecimal.valueOf(200))
                );
    }
}