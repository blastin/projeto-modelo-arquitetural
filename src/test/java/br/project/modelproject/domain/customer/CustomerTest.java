package br.project.modelproject.domain.customer;

import br.project.modelproject.domain.order.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

class CustomerTest {

    @Test
    @DisplayName("Quando CPF invalido deve subir exceção CustomerInvalidException")
    void quandoCpfInvalidoDeveSubirExcecaoCustomerException() {

        final String cpf = "735959570321";

        final CustomerInvalidException ex =
                Assertions
                        .assertThrows(CustomerInvalidException.class, () -> new Customer(cpf));

        Assertions.assertEquals(2932, ex.toInt());

        Assertions.assertEquals(String.format("CPF (%s) incorreto", cpf), ex.getMessage());

    }

    @Test
    @DisplayName("Quando cliente não é encontrado pelo CPF deve subir exceção CustomerNotFoundException")
    void quandoClienteNaoEncontradoDeveSubirExcecaoCustomerNotFoundException() {

        final Customer mock = Customer.mock();

        final CustomerNotFoundException ex = Assertions.assertThrows
                (
                        CustomerNotFoundException.class,
                        () -> mock
                                .overdueCustomerOrders(cpf -> false, null)
                );

        Assertions.assertEquals(4384, ex.toInt());

        Assertions.assertEquals(String.format("cliente (%s) não foi encontrado", mock.getCpf()), ex.getMessage());

    }

    @Test
    @DisplayName("Quando Uma ordem em atraso é retornada")
    void quandoUmaOrdemEmAtrasoEhRetornada() {

        final Order mock = Order.mock();

        final Collection<Order> ordens =
                Customer
                        .mock()
                        .overdueCustomerOrders
                                (
                                        cpf -> true,
                                        cpf -> Collections.singletonList(mock)
                                );

        final Optional<Order> orderOptional = ordens.stream().findFirst();

        Assertions.assertTrue(orderOptional.isPresent());

        Assertions.assertEquals(mock, orderOptional.get());

    }

    @Test
    @DisplayName("Quando cliente não encontrado, salvando")
    void quandoClienteNaoEncontradoSalvando() {

        final CustomersCommand mock = Mockito.mock(CustomersCommand.class);

        Assertions.assertDoesNotThrow(() -> Customer.mock().save(mock, cpf -> false));

        Mockito
                .verify(mock, Mockito.times(1))
                .save(Mockito.any(Customer.class));

    }

    @Test
    @DisplayName("Quando cliente encontrado, subir excessao")
    void quandoClienteEncontradoSubirExcecao() {

        final Customer mock = Customer.mock();

        final CustomerFoundException ex = Assertions.assertThrows
                (
                        CustomerFoundException.class,
                        () -> mock
                                .save
                                        (
                                                customer ->
                                                {
                                                    throw new RuntimeException();
                                                }
                                                , cpf -> true
                                        )
                );

        Assertions.assertEquals(2912, ex.toInt());

        Assertions.assertEquals(String.format("Cliente com cpf (%s) foi encontrado", mock.getCpf()), ex.getMessage());

    }
}