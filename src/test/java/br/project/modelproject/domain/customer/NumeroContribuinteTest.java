package br.project.modelproject.domain.customer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.function.Predicate;

class NumeroContribuinteTest {

    final Predicate<String> predicado = new NumeroContribuinte.PredicadoCPF();

    @Test
    void cenarioCadeiaComPrimeiroDigitoVerificadorErrado() {
        Assertions.assertFalse(predicado.test("219.786.850-19"));
    }

    @Test
    void cenarioCadeiaComSegundoDigitoVerificadorErrado() {
        Assertions.assertFalse(predicado.test("219.786.850-08"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"219.786.850-09", "73595957032"})
    void cenarioCPFValido(final String cpf) {
        Assertions.assertTrue(predicado.test(cpf));
    }

    @ParameterizedTest
    @ValueSource
            (strings =
                    {
                            "",
                            "735959570321",
                            "000.000.000-00",
                            "111.111.111-11",
                            "222.222.222-22",
                            "333.333.333-33",
                            "444.444.444-44",
                            "55555555555",
                            "666.666.666-66",
                            "777.777.777-77",
                            "888.888.888-88",
                            "999.999.999-99"
                    }
            )
    void contribuinteTipoCPFInvalidos(final String cpf) {
        Assertions.assertFalse(predicado.test(cpf));
    }

    @Test
    void contribuinteTipoCpfComInstanciaNula() {
        Assertions.assertFalse(predicado.test(null));
    }

}