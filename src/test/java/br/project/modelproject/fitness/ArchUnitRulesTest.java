package br.project.modelproject.fitness;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import com.tngtech.archunit.lang.syntax.elements.ClassesShouldConjunction;
import com.tngtech.archunit.library.Architectures;

@SuppressWarnings("unused")
@AnalyzeClasses(packages = "br.project.modelproject", importOptions = ImportOption.DoNotIncludeTests.class)
class ArchUnitRulesTest {

    @ArchTest
    static final Architectures.LayeredArchitecture ARCHITECTURES =
            Architectures
                    .layeredArchitecture()
                    .layer("Dominio").definedBy("br.project.modelproject.domain..")
                    .layer("Aplicação").definedBy("br.project.modelproject.application..")
                    .layer("Adaptador").definedBy("br.project.modelproject.adapter..")
                    .layer("Web").definedBy("br.project.modelproject.adapter.web..")
                    .layer("PubSub").definedBy("br.project.modelproject.adapter.pubsub..")
                    .layer("Client").definedBy("br.project.modelproject.adapter.client..")
                    .layer("Repository").definedBy("br.project.modelproject.adapter.repository..")
                    .whereLayer("Web").mayNotBeAccessedByAnyLayer()
                    .whereLayer("PubSub").mayNotBeAccessedByAnyLayer()
                    .whereLayer("Client").mayNotBeAccessedByAnyLayer()
                    .whereLayer("Repository").mayNotBeAccessedByAnyLayer()
                    .whereLayer("Aplicação").mayOnlyBeAccessedByLayers("Adaptador")
                    .whereLayer("Dominio").mayOnlyBeAccessedByLayers("Adaptador", "Aplicação");

    @ArchTest
    static final ClassesShouldConjunction DOMINIO_RULES =
            ArchRuleDefinition
                    .classes()
                    .that()
                    .resideInAPackage("br.project.modelproject.domain..")
                    .should()
                    .onlyDependOnClassesThat()
                    .resideInAnyPackage("java..", "br.project.modelproject.domain..");

    @ArchTest
    static final ClassesShouldConjunction APLICACAO_RULES =
            ArchRuleDefinition
                    .classes()
                    .that()
                    .resideInAPackage("br.project.modelproject.application..")
                    .should()
                    .onlyDependOnClassesThat()
                    .resideInAnyPackage
                            (
                                    "java..",
                                    "br.project.modelproject.application..",
                                    "br.project.modelproject.domain.."
                            );

}
