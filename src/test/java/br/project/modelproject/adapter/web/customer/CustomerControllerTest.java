package br.project.modelproject.adapter.web.customer;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("test")
class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Quando CPF invalido deve retornar mensagem de error")
    void quandoCpfInvalidoDeveRetornarMensagemDeError() throws Exception {
        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders
                                        .get("/customers/{cpf}/orders", "317.517.370-50")
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:br/project/modelproject/adapter/web/customer/resposta-cpf-invalido.json")
                                                .toPath()
                                )
                ));
    }

    @Test
    @DisplayName("Quando Cliente não encontrado deve retornar mensagem de error")
    void quandoClienteNaoEncontradoDeveRetornarMensagemError() throws Exception {
        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders
                                        .get("/customers/{cpf}/orders", "964.464.640-10")
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:br/project/modelproject/adapter/web/customer/resposta-cliente-nao-encontrado.json")
                                                .toPath()
                                )
                ));

    }

    @Test
    @DisplayName("Order client API retorna uma coleção vazia, a resposta deve ser uma lista vazia")
    @Sql("cliente-para-order-vazia.sql")
    void quandoOrderClientAPIRetornaUmaColecaoVaziaARespostaDeveSerUmaListaVazia() throws Exception {

        final String cpf = "125.724.380-20";

        WireMock.stubFor
                (
                        WireMock
                                .get("/orders/".concat(cpf))
                                .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                .willReturn
                                        (
                                                WireMock
                                                        .aResponse()
                                                        .withHeader("Content-type", MediaType.APPLICATION_JSON_VALUE)
                                                        .withBodyFile("order-client-resposta-colecao-vazia.json")
                                        )
                );

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders
                                        .get("/customers/{cpf}/orders", cpf)
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:br/project/modelproject/adapter/web/customer/resposta-colecao-vazia.json")
                                                .toPath()
                                )
                ));

    }

    @Test
    @DisplayName("Order client API retorna uma coleção, então deve retornar uma resposta")
    @Sql("cliente-para-order.sql")
    void quandoOrderClientAPIRetornaUmaColecaoEntaoDeveRetornarResposta() throws Exception {

        final String cpf = "578.647.570-06";

        final String json = new String
                (
                        Files
                                .readAllBytes
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:__files/ordens-resposta.json")
                                                        .toPath()
                                        )
                );

        final String jsonComDataAtual = String.format(json, LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE));

        WireMock.stubFor
                (
                        WireMock
                                .get("/orders/".concat(cpf))
                                .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                .willReturn
                                        (
                                                WireMock
                                                        .aResponse()
                                                        .withHeader("Content-type", MediaType.APPLICATION_JSON_VALUE)
                                                        .withBody(jsonComDataAtual)
                                        )
                );

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders
                                        .get("/customers/{cpf}/orders", cpf)
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:br/project/modelproject/adapter/web/customer/resposta-ordens.json")
                                                .toPath()
                                )
                ));

    }

    @Test
    @DisplayName("Quando order API retorna ordens não encontrada, deve retornar mensagem de error")
    @Sql("cliente-para-ordens-nao-encontrada.sql")
    void quandoOrderAPIRetornaOrdensNaoEncontradaDeveSubirExcecao() throws Exception {

        final String cpf = "131.763.630-95";

        WireMock.stubFor
                (
                        WireMock
                                .get("/orders/".concat(cpf))
                                .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                .willReturn
                                        (
                                                WireMock
                                                        .aResponse()
                                                        .withHeader("Content-type", MediaType.APPLICATION_JSON_VALUE)
                                                        .withStatus(HttpStatus.NOT_FOUND.value())
                                        )
                );

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders
                                        .get("/customers/{cpf}/orders", cpf)
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:br/project/modelproject/adapter/web/customer/resposta-ordens-nao-encontrada.json")
                                                .toPath()
                                )
                ));

    }

    @Test
    @DisplayName("Quando orders API estiver indisponivel, deve retornar mensagem de error")
    @Sql("cliente-para-ordens-indisponivel.sql")
    void quandoOrdersAPIEstiverIndisponivelDeveRetornarListaVazia() throws Exception {

        final String cpf = "19196173025";

        WireMock.stubFor
                (
                        WireMock
                                .get("/orders/".concat(cpf))
                                .withHeader("Content-Type", WireMock.equalTo(MediaType.APPLICATION_JSON_VALUE))
                                .willReturn
                                        (
                                                WireMock
                                                        .aResponse()
                                                        .withHeader("Content-type", MediaType.APPLICATION_JSON_VALUE)
                                                        .withStatus(HttpStatus.SERVICE_UNAVAILABLE.value())
                                        )
                );

        mockMvc
                .perform
                        (
                                MockMvcRequestBuilders
                                        .get("/customers/{cpf}/orders", cpf)
                                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity())
                .andExpect(MockMvcResultMatchers.content().json(Files
                        .readString
                                (
                                        ResourceUtils
                                                .getFile("classpath:br/project/modelproject/adapter/web/customer/resposta-ordens-nao-encontrada.json")
                                                .toPath()
                                )
                ));

    }
}