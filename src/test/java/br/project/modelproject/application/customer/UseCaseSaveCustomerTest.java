package br.project.modelproject.application.customer;

import br.project.modelproject.domain.customer.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

class UseCaseSaveCustomerTest {

    @Test
    @DisplayName("Quando recebe evento para salvar cliente")
    void quandoRecebeEventoParaSalvarCliente() {

        final AtomicBoolean acessado = new AtomicBoolean(false);

        Assertions.assertDoesNotThrow
                (
                        () -> new UseCaseSaveCustomerImpl(customer -> false, customer -> acessado.set(true))
                                .save
                                        (
                                                () ->
                                                        Customer
                                                                .mock()
                                                                .getCpf()
                                        )
                );

        Assertions.assertTrue(acessado.get());

    }
}