package br.project.modelproject.application.customer;

import br.project.modelproject.domain.customer.Customer;
import br.project.modelproject.domain.order.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

class UseCaseOverdueCustomerOrderTest {

    @Test
    @DisplayName("Quando cliente não possui ordem, deve retornar uma coleção vazia")
    void quandoClienteNaoPossuiOrdemDeveRetornarUmaColecaoVazia() {
        Assertions.assertTrue
                (
                        new UseCaseOverdueCustomerOrderImpl(cpf -> true, cpf -> Collections.emptyList())
                                .withValueAboveTheLimit(Customer.mock().getCpf())
                                .isEmpty()
                );
    }

    @Test
    @DisplayName("Quando cliente possui ordens, porém abaixo do limite. Deve retornar uma coleção vazia")
    void quandoClientePossuiOrdemPoremAbaixoDoLimiteDeveRetornarUmaColecaoVazia() {
        Assertions.assertTrue
                (
                        new UseCaseOverdueCustomerOrderImpl
                                (
                                        cpf -> true,
                                        cpf -> // String -> Collection<Order>
                                                Collections
                                                        .singletonList
                                                                (
                                                                        Order.mock()
                                                                                .toBuilder()
                                                                                .withValue(BigDecimal.valueOf(150))
                                                                                .build()
                                                                )
                                )
                                .withValueAboveTheLimit(Customer.mock().getCpf())
                                .isEmpty()
                );
    }

    @Test
    @DisplayName("Quando cliente possui pelo menos uma ordem acima do limite. Deve retornar uma coleção")
    void quandoClientePossuiPeloMenosUmaOrdemAcimaDoLimiteDeveRetornarUmaColecaoVazia() {

        final Order mock = Order.mock();

        final BigDecimal valueAboveTheLimit = BigDecimal.valueOf(100);

        final Collection<OverdueCustomerOrderResponse> ordens = new UseCaseOverdueCustomerOrderImpl
                (
                        cpf -> true,
                        cpf -> // String -> Collection<Order>
                                List.of
                                        (
                                                mock,
                                                mock
                                                        .toBuilder()
                                                        .withValue(valueAboveTheLimit)
                                                        .build()
                                        )

                )
                .withValueAboveTheLimit(Customer.mock().getCpf());

        final Optional<OverdueCustomerOrderResponse> first = ordens.stream().findFirst();

        Assertions.assertTrue(first.isPresent());

        final OverdueCustomerOrderResponse overdueCustomerOrderResponse = first.get();

        Assertions.assertNotEquals(valueAboveTheLimit, overdueCustomerOrderResponse.getValue());

        Assertions.assertEquals(mock.getValue(), overdueCustomerOrderResponse.getValue());

        Assertions.assertEquals(mock.getDescription(), overdueCustomerOrderResponse.getDescription());

    }
}