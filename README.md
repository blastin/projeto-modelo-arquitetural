# Projeto Modelo Arquitetural

## Modelo

![arquitetura](images/model.png)

*Imagem 1: Diagrama de Componentes*

## Commandos

### Build Project

~~~~shell
./mvnw clean package
~~~~

### Build Image

~~~~shell
docker build -t model-project-image .
~~~~

### Criando container

~~~~shell
docker run -it --rm -p 8080:8080 -e ENVIRONMENT_PROFILE=default model-project-image
~~~~

### Inicializando Testes de Integração

~~~shell
docker-compose -f docker-compose.yml up --build --exit-code-from detalhe -V
docker-compose -f docker-compose.yml down
~~~

Observação: Antes de inicializar os testes de integração faça um __build__ do projeto

## Google Cloud PUB/SUB

O Pub/Sub permite que os serviços se comuniquem de forma assíncrona, com latências na ordem de 100 milissegundos.

O Pub/Sub é usado para streaming de análises e pipelines de integração de dados para ingerir e distribuir dados. É
igualmente eficaz como middleware orientado a mensagens para integração do serviço ou como uma fila para carregar
tarefas em paralelo.

### Exemplo

### Dependências no Spring

| Dependência                           |   Informação                                                           |
|   ---                                 |        ---                                                             |
| spring-cloud-gcp-dependencies         | Coleção de bibliotecas para facilitar o uso da plataforma Google Cloud |           |
| spring-cloud-gcp-starter-pubsub       | Biblioteca que facilita integração com pub/sub GCP                     |

### Exemplos de código

#### Consumindo mensagem

~~~java

package br.exemplo.adapter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class Application {

    public static void main(final String[] args) {
        SpringApplication.run(ModelProjectApplication.class, args);
    }

    /**
     * Para cada comunicação deve ser gerado uma nova instância.
     * O nome do método identifica um novo canal
     * @return canal de comunicação pub/sub
     */
    @Bean
    public MessageChannel inputMessageChannel() {
        return new PublishSubscribeChannel();
    }


    /**
     *
     * @param messageChannel Instância do Canal de Comunicação
     * @param pubSubTemplate Template Para Comunicação com Google Cloud
     * @param topic Identificador do tópico
     * @return Prepara Adaptador para canal de entrada de dados do tópico
     */
    @Bean
    public PubSubInboundChannelAdapter inboundChannelAdapter(
            @Qualifier("inputMessageChannel") MessageChannel messageChannel,
            PubSubTemplate pubSubTemplate,
            @Value("${order.topic}") final String topic) {
        PubSubInboundChannelAdapter adapter =
                new PubSubInboundChannelAdapter(pubSubTemplate, topic);
        adapter.setOutputChannel(messageChannel);
        adapter.setAckMode(AckMode.MANUAL);
        adapter.setPayloadType(String.class);
        return adapter;
    }

    /**
     * Porta de entrada do aplicativo com mensageria
     * @param payload Dados trafegados pelo tópico no tipo String
     * @param message Estrutura de dados com dados de cabeçalho da mensagem
     */
    @ServiceActivator(inputChannel = "inputMessageChannel")
    public void messageReceiver(
            String payload,
            @Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) BasicAcknowledgeablePubsubMessage message) {
        LOGGER.info("Mensagem recebido pelo tópico (customer.topic). Payload: " + payload);
        message.ack();
    }
}
~~~

##### Informativo

1. Criar Canal Mensagem [ Receber ]
2. Criar Adaptador Canal [ Entrada (Inbound)]
3. Criar Método para reagir a um evento __(Recebe um Payload::String)__

#### Produzindo Mensagem

~~~java

package br.exemplo.adapter;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class Application {

    public static void main(final String[] args) {
        SpringApplication.run(ModelProjectApplication.class, args);
    }

    /**
     * Para cada comunicação deve ser gerado uma nova instância.
     * O nome do método identifica um novo canal
     * @return canal de comunicação pub/sub
     */
    @Bean
    public MessageChannel outputMessageChannel() {
        return new PublishSubscribeChannel();
    }

    /**
     *
     * @param pubsubTemplate Template Para Comunicação com Google Cloud
     * @param topic Identificador do tópico
     * @return Canal de comunicação para saída de informações para um tópico específico
     */
    @Bean
    @ServiceActivator(inputChannel = "outputMessageChannel")
    public MessageHandler messageSender(PubSubTemplate pubsubTemplate, @Value("${customer.topic}") final String topic) {
        return new PubSubMessageHandler(pubsubTemplate, topic);
    }

    /**
     * Interface fornecida para encaminhar mensagem.
     * Proposta parecida com RMI (Remote Method Invocation )
     */
    @MessagingGateway(defaultRequestChannel = "outputMessageChannel")
    interface PubsubOutboundGateway {
        void sendToPubsub(String text);
    }

}

~~~

##### Informativo

1. Criar Canal Mensagem [ Receber ]
2. Criar Adaptador Canal [ Entrada (Inbound)]
3. Criar Método para reagir a um evento __(Recebe um Payload::String)__

---

### Ferramentas

[Google Cloud SDK](https://hub.docker.com/r/google/cloud-sdk)

#### Criando um container docker

~~~shell
docker run -it --rm -p 8085:8085 -e PROJECT_ID=exemplo google/cloud-sdk:alpine
~~~

Devemos gerar uma exposição da porta 8085 para permitir acesso externo ao PUB/SUB

#### Instalando OPENJDK-11

~~~shell
apk update && apk add openjdk11-jdk
~~~~

Inicializando emulação de pubsub para um projeto identificado pela variável de ambiente

~~~shell
gcloud components install pubsub-emulator beta --quiet
gcloud beta emulators pubsub start --project=$PROJECT_ID --quiet
~~~

#### Dockerfile de PUB/SUB Emulator

Para rodar emulador PUB/SUB da Google Cloud, podemos criar uma imagem docker
utilizando o arquivo abaixo
~~~dockerfile
FROM google/cloud-sdk:alpine

# Atualizando Alpine e Instalando Java 11
RUN apk update && apk add openjdk11-jdk

# Baixando componentes do Google Cloud
RUN gcloud components install pubsub-emulator beta --quiet

# Comando de inicialização do container
CMD gcloud config set project $PROJECT_ID && gcloud beta emulators pubsub start --project $PROJECT_ID --quiet
~~~

#### Arquivo Dockerfile

[Dockerfile](google-cloud/pubsub/Dockerfile)

#### Gerando Imagem Docker

~~~shell
docker build -t google-cloud-pubsub:alpine-11-jdk google-cloud
~~~

#### Rodando PUB/SUB

~~~shell
docker run -it --rm -p 8085:8085 -e PROJECT_ID=customer google-cloud-pubsub:alpine-11-jdk
~~~

## Referências

*Google Cloud. 2021. O que é o Pub/Sub? | Documentação do Cloud Pub/Sub | Google Cloud. [online]
Available at: <https://cloud.google.com/pubsub/docs/overview?hl=pt-br> [Accessed 12 July 2021].*

*Gitlab. 2021. Keyword reference for the .gitlab-ci.yml file. [online]
Available at: <https://docs.gitlab.com/ee/ci/yaml/> [Accessed 12 July 2021].*

*Docker Documentation. 2021. Compose file version 3 reference. [online]
Available at: <https://docs.docker.com/compose/compose-file/compose-file-v3/> [Accessed 12 July 2021].*

*Spring.io. 2021. Messaging with Google Cloud Pub/Sub. [online]
Available at: <https://spring.io/guides/gs/messaging-gcp-pubsub/> [Accessed 12 July 2021].*