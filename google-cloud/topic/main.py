import sys

from google.cloud import pubsub_v1


def publish_messages(project_id, topic_id):
    """Publishes message to a Pub/Sub topic."""

    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_id)

    data = '{"cpf" : "10165182008" }'

    data = data.encode("utf-8")

    future = publisher.publish(topic_path, data)

    print(f"Published {future.result()} messages to {topic_path}.")


if __name__ == '__main__':
    publish_messages(sys.argv[1], sys.argv[2])
