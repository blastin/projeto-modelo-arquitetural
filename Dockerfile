FROM adoptopenjdk/openjdk16-openj9:alpine

RUN adduser usuario -D

USER usuario

WORKDIR /home/usuario

COPY target/*.jar projeto.jar

CMD ["java","-jar","-XX:+UseContainerSupport","-XX:+UseG1GC","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=${ENVIRONMENT_PROFILE}","projeto.jar"]